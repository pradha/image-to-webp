from PIL import Image
import PIL
import os
import glob
import imghdr

files = os.listdir('images/')
print("These is are file in directory: ")
for file in files:
    if imghdr.what(f"images/{file}"):
        print(f"{file} this is an image, can be converted")
    else:
        print(f"{file} this is not an image, will not be converted")

if len(files)>=1:
    execute = input(f"\nDo you want to convert {len(files)} file(s) into next generation image format (.webp)? (y/n) : ")
    while not(execute=="y" or execute=="Y" or execute=="yes" or execute=="Yes" or execute=="YES" or execute=="n" or execute=="N" or execute=="no" or execute=="No" or execute=="NO"):
        print('Please input "y" for yes or "n" to no')
        execute = input(f"Do you want to convert {len(files)} file(s) into next generation image format (.webp)? (y/n) : ")
    if execute=="y" or execute=="Y" or execute=="yes" or execute=="Yes" or execute=="YES":
        print('\nExecuting...')
        for file in files:
            if imghdr.what(f"images/{file}"):
                print(f'procesing file {file}...')
                filename = file.rsplit('.', 1)[0]
                image = Image.open(f'images/{file}')
                image.save(f'results/{filename}.webp', 'webp')
                print(f'- file {file} has been converted to {filename}.webp')
            else:
                print(f'{file} is not an image file, ignored...')
        
        delete = input(f"\nAll file(s) has been successfully converted\nDo you want to delete all source of file(s)? (y/n) : ")
        while not(delete=="y" or delete=="Y" or delete=="yes" or delete=="Yes" or delete=="YES" or delete=="n" or delete=="N" or delete=="no" or delete=="No" or delete=="NO"):
            print('Please input "y" for yes or "n" to no')
            delete = input(f"Do you want to delete all source of file(s)? (y/n) : ")
        if delete=="y" or delete=="Y" or delete=="yes" or delete=="Yes" or delete=="YES":
            for file in files:
                os.remove(f"images/{file}")
        print(f'Application closed!')
else:
    print("no image file detected")