# Image to Webp converter
You can edit any standard image files like jpg or png into webp next generation image format with this script. This is using pillow module.
## How to use
Create folder `images` and place all your source image that you want to convert, then create folder `results`. This folder will be output of converted images
Just run `python convert.py`
